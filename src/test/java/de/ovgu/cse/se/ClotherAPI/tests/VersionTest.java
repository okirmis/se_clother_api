package de.ovgu.cse.se.ClotherAPI.tests;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.logging.Logger;

import org.junit.Test;

import de.ovgu.cse.se.ClotherAPI.exceptions.VersionCouldNotbeResolvedException;
import de.ovgu.cse.se.ClotherAPI.helper.IVersionHelper;
import de.ovgu.cse.se.ClotherAPI.helper.VersionHelper;

public class VersionTest {

	@Test
	public void getLocalVersionTest() throws MalformedURLException, VersionCouldNotbeResolvedException, IOException {
		IVersionHelper helper = new VersionHelper();
		String version = helper.getLocalVersion();
		Logger.getLogger("de.ovgu.cse.se.clotherAPI").warning("VersionTest - Local Version " + version);
	}
	
	@Test
	public void getOnlineVersionTest() throws MalformedURLException, VersionCouldNotbeResolvedException, IOException {
		IVersionHelper helper = new VersionHelper();
		String version = helper.getOnlineVersion();
		Logger.getLogger("de.ovgu.cse.se.clotherAPI").warning("VersionTest - Remote Version " + version);
	}
	
}
