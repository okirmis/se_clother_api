package de.ovgu.cse.se.ClotherAPI.tests;

import java.util.logging.Logger;

import org.junit.After;

import de.ovgu.cse.se.ClotherAPI.IObjectProvider;
import de.ovgu.cse.se.ClotherAPI.ConfigurationContext;
import de.ovgu.cse.se.ClotherAPI.ObjectProviderFactory;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserdataNotCorrectException;
import de.ovgu.cse.se.ClotherAPI.helper.EntityGenerator;
import de.ovgu.cse.se.ClotherAPI.models.User;

public abstract class AbstractTest {

	public IObjectProvider provider;
	public EntityGenerator generator;

	public User user;

	public AbstractTest() {
		provider = ObjectProviderFactory
				.getObjectProvider(ConfigurationContext.TEST);
		generator = new EntityGenerator();
	}

	public void authenticate() throws UserdataNotCorrectException {
		if (provider.getUser() == null) {
			Logger.getLogger("de.ovgu.cse.se.clotherapi").info("Not authenticated yet. creasting test user and logging in");
			String password = "password";
			User user = generator.getLastUser();
			user.setPassword(password);
			try {
				provider.addUser(user);
			} catch (UserNotAddedException e) {
				// User exists already. Everything is ok.
			}
			
			//try 3 times to authenticate
			
			try {
				this.user = provider.authenticate(user.getEmail(), password);
			} catch (UserdataNotCorrectException e) {
				try {
					Thread.sleep(1000);
					this.user = provider
							.authenticate(user.getEmail(), password);
				} catch (UserdataNotCorrectException e1) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
					this.user = provider
							.authenticate(user.getEmail(), password);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
	}

	@After
	public void after() {
		ObjectProviderFactory.getObjectProvider().closeConnection();
	}
}
