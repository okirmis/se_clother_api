package de.ovgu.cse.se.ClotherAPI.tests;

import org.junit.Assert;

import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserdataNotCorrectException;
import de.ovgu.cse.se.ClotherAPI.models.User;

public class AuthentificationTest extends AbstractTest{

	//@Test
	public void authenticateUserTest() throws UserNotAddedException,
			UserdataNotCorrectException {
		User user = generator.generateNewUser();

		provider.addUser(user);

		User authenticatedUser = provider.authenticate(user.getEmail(),
				user.getPassword());

		Assert.assertEquals(
				"The authenticated user does not match the created user", user,
				authenticatedUser);
	}
}
