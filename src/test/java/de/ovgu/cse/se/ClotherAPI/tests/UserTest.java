package de.ovgu.cse.se.ClotherAPI.tests;

import org.junit.Test;

import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotAuthenticatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotDeletedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotUpdatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserdataNotCorrectException;
import de.ovgu.cse.se.ClotherAPI.models.User;

public class UserTest extends AbstractTest {

	@Test
	public void createUserTest() throws UserNotAddedException {
		User user = generator.generateNewUser();

		provider.addUser(user);
	}

	@Test(expected = UserNotAddedException.class)
	public void noEqualEmailTest() throws UserNotAddedException {
		String email = "test//@Test.de";
		
		User user = generator.generateNewUser();
		User user2 = generator.generateNewUser();
		user.setEmail(email);
		user2.setEmail(email);

		provider.addUser(user);
		provider.addUser(user2);
		// expect exception here

	}
	
	@Test
	public void updateUserTest() throws UserNotUpdatedException,
			UserdataNotCorrectException, UserNotAddedException, UserNotAuthenticatedException {
		authenticate();
		user.setCreditscore(20);
		provider.updateUser();
	}
	
	@Test
	public void removeUserTest() throws UserNotDeletedException, UserNotAddedException, UserdataNotCorrectException, UserNotAuthenticatedException {
		authenticate();
		provider.deleteUser();
	}

}
