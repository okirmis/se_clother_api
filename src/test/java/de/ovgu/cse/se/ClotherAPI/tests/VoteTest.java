package de.ovgu.cse.se.ClotherAPI.tests;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotAuthenticatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserdataNotCorrectException;
import de.ovgu.cse.se.ClotherAPI.exceptions.VoteNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.VoteNotFoundException;
import de.ovgu.cse.se.ClotherAPI.models.Vote;

public class VoteTest extends AbstractTest {

	@Test
	public void createVoteTest() throws VoteNotAddedException, UserNotAuthenticatedException, UserNotAddedException, UserdataNotCorrectException {
		Vote vote = generator.generateNewVote();
		authenticate();
		provider.addVote(vote);
	}
	
	//@Test - no idea how to get the id
	/*public void getVoteTest() throws UserdataNotCorrectException, VoteNotAddedException, UserNotAuthenticatedException, UserNotAddedException, VoteNotFoundException {
		authenticate();
		Vote vote = generator.generateNewVote();
		provider.addVote(vote);
		
		vote = provider.getVote(vote.getName());
		Assert.assertNotNull(vote);
	}*/
	
	//@Test
	public void getOwnVotesTest() throws UserdataNotCorrectException, VoteNotAddedException, UserNotAuthenticatedException, UserNotAddedException, VoteNotFoundException {
		authenticate();
		Vote vote = generator.generateNewVote();
		vote.setCreator(provider.getUser());
		provider.addVote(vote);
		List<Vote> votes = provider.getOwnVotes();
		Assert.assertNotNull(votes);
	}
	
	/*@Test
	public void updateVoteTest() throws VoteNotUpdatedException,
			UserdataNotCorrectException, VoteNotAddedException, UserNotAuthenticatedException, UserNotAddedException, VoteNotFoundException {
		authenticate();
		Vote vote = generator.generateNewVote();
		provider.addVote(vote);
		
		vote = provider.getVote(vote.getName());
		vote.setName(vote.getName() + "updated");
		provider.updateVote(vote);
	}*/
}
