package de.ovgu.cse.se.ClotherAPI.tests;

import org.junit.Test;

import de.ovgu.cse.se.ClotherAPI.exceptions.OccasionNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.OccasionNotFoundException;
import de.ovgu.cse.se.ClotherAPI.exceptions.OccasionNotUpdatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.PictureNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.PictureNotDeletedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.PictureNotFoundException;
import de.ovgu.cse.se.ClotherAPI.exceptions.TagNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.TagNotFoundException;
import de.ovgu.cse.se.ClotherAPI.exceptions.TagNotUpdatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.TypeNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.TypeNotFoundException;
import de.ovgu.cse.se.ClotherAPI.exceptions.TypeNotUpdatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotAuthenticatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotUpdatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.VoteNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.VoteNotDeletedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.VoteNotFoundException;
import de.ovgu.cse.se.ClotherAPI.exceptions.VoteNotUpdatedException;

public class SecurityTest extends AbstractTest {
	
	@Test(expected = UserNotAuthenticatedException.class)
	public void updateUserNotAccessableWithoutAuthentification()
			throws UserNotUpdatedException, UserNotAuthenticatedException {
		provider.updateUser();
	}

	@Test(expected = UserNotAuthenticatedException.class)
	public void addPictureNotAccessableWithoutAuthentification()
			throws PictureNotAddedException, UserNotAuthenticatedException {
		provider.addPicture(generator.generateNewPicture());
	}

	@Test(expected = UserNotAuthenticatedException.class)
	public void deletePictureNotAccessableWithoutAuthentification()
			throws PictureNotDeletedException, UserNotAuthenticatedException {
		provider.deletePicture(generator.getLastPicture());
	}

	@Test(expected = UserNotAuthenticatedException.class)
	public void getPictureNotAccessableWithoutAuthentification()
			throws PictureNotFoundException, UserNotAuthenticatedException {
		provider.getPicture(generator.getLastPicture().getId());
	}

	@Test(expected = UserNotAuthenticatedException.class)
	public void getPicturesNotAccessableWithoutAuthentification()
			throws PictureNotFoundException, UserNotAuthenticatedException {
		provider.getPictures();
	}

	@Test(expected = UserNotAuthenticatedException.class)
	public void getOccasionNotAccessableWithoutAuthentification()
			throws OccasionNotFoundException, UserNotAuthenticatedException {
		provider.getOccasion(generator.getLastOccasion().getName());
	}

	@Test(expected = UserNotAuthenticatedException.class)
	public void getOccasionsNotAccessableWithoutAuthentification()
			throws OccasionNotFoundException, UserNotAuthenticatedException {
		provider.getOccasions();
	}

	@Test(expected = UserNotAuthenticatedException.class)
	public void addOccasionsNotAccessableWithoutAuthentification()
			throws OccasionNotAddedException, UserNotAuthenticatedException {
		provider.addOccasion(generator.generateNewOccasion());
	}

	@Test(expected = UserNotAuthenticatedException.class)
	public void getTypesNotAccessableWithoutAuthentification()
			throws TypeNotFoundException, UserNotAuthenticatedException {
		provider.getTypes();
	}

	@Test(expected = UserNotAuthenticatedException.class)
	public void getTypeNotAccessableWithoutAuthentification()
			throws TypeNotFoundException, UserNotAuthenticatedException {
		provider.getType(generator.getLastType().getName());
	}

	@Test(expected = UserNotAuthenticatedException.class)
	public void addTypeNotAccessableWithoutAuthentification()
			throws TypeNotAddedException, UserNotAuthenticatedException {
		provider.addType(generator.generateNewType());
	}

	@Test(expected = UserNotAuthenticatedException.class)
	public void getTagsNotAccessableWithoutAuthentification()
			throws TagNotFoundException, UserNotAuthenticatedException {
		provider.getTags();
	}

	@Test(expected = UserNotAuthenticatedException.class)
	public void getTagNotAccessableWithoutAuthentification()
			throws TagNotFoundException, UserNotAuthenticatedException {
		provider.getTag(generator.getLastTag().getName());
	}

	@Test(expected = UserNotAuthenticatedException.class)
	public void addTagNotAccessableWithoutAuthentification()
			throws TagNotAddedException, UserNotAuthenticatedException {
		provider.addTag(generator.generateNewTag());
	}

	@Test(expected = UserNotAuthenticatedException.class)
	public void getOwnVotesNotAccessableWithoutAuthentification()
			throws VoteNotFoundException, UserNotAuthenticatedException {
		provider.getOwnVotes();
	}

	@Test(expected = UserNotAuthenticatedException.class)
	public void getVoteNotAccessableWithoutAuthentification()
			throws VoteNotFoundException, UserNotAuthenticatedException {
		provider.getVote(generator.getLastVote().getId());
	}

	@Test(expected = UserNotAuthenticatedException.class)
	public void addVoteToPictureNotAccessableWithoutAuthentification()
			throws VoteNotAddedException, UserNotAuthenticatedException {
		provider.addVote(generator.generateNewVote());
	}

	@Test(expected = UserNotAuthenticatedException.class)
	public void updateVoteNotAccessableWithoutAuthentification()
			throws VoteNotUpdatedException, UserNotAuthenticatedException {
		provider.updateVote(generator.generateNewVote());
	}

	@Test(expected = UserNotAuthenticatedException.class)
	public void updateOccasionNotAccessableWithoutAuthentification()
			throws OccasionNotUpdatedException, UserNotAuthenticatedException {
		provider.updateOccasion(generator.generateNewOccasion());
	}

	@Test(expected = UserNotAuthenticatedException.class)
	public void updateTypeNotAccessableWithoutAuthentification()
			throws TypeNotUpdatedException, UserNotAuthenticatedException {
		provider.updateType(generator.generateNewType());
	}

	@Test(expected = UserNotAuthenticatedException.class)
	public void updateTagNotAccessableWithoutAuthentification()
			throws TagNotUpdatedException, UserNotAuthenticatedException {
		provider.updateTag(generator.generateNewTag());
	}

	@Test(expected = UserNotAuthenticatedException.class)
	public void removeVoteNotAccessableWithoutAuthentification()
			throws VoteNotDeletedException, UserNotAuthenticatedException {
		provider.deleteVote(generator.getLastVote());
	}
}
