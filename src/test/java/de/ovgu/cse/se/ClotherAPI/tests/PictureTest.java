package de.ovgu.cse.se.ClotherAPI.tests;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import de.ovgu.cse.se.ClotherAPI.exceptions.PictureNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.PictureNotFoundException;
import de.ovgu.cse.se.ClotherAPI.exceptions.PictureNotUpdatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotAuthenticatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserdataNotCorrectException;
import de.ovgu.cse.se.ClotherAPI.models.Picture;

public class PictureTest extends AbstractTest {

	@Test
	public void createPictureTest() throws PictureNotAddedException, UserNotAuthenticatedException, UserNotAddedException, UserdataNotCorrectException {
		Picture picture = generator.generateNewPicture();
		authenticate();
		provider.addPicture(picture);
	}
	
	@Test
	public void getPictureTest() throws UserdataNotCorrectException, PictureNotAddedException, UserNotAuthenticatedException, UserNotAddedException, PictureNotFoundException {
		authenticate();
		Picture picture = generator.generateNewPicture();
		provider.addPicture(picture);
		
		picture = provider.getPicture(picture.getName());
		Assert.assertNotNull(picture);
	}
	
	@Test
	public void getPicturesTest() throws UserdataNotCorrectException, PictureNotAddedException, UserNotAuthenticatedException, UserNotAddedException, PictureNotFoundException {
		authenticate();	
		List<Picture> pictures = provider.getPictures();
		Assert.assertNotNull(pictures);
	}
	
	@Test
	public void updatePictureTest() throws PictureNotUpdatedException,
			UserdataNotCorrectException, PictureNotAddedException, UserNotAuthenticatedException, UserNotAddedException, PictureNotFoundException {
		authenticate();
		Picture picture = generator.generateNewPicture();
		provider.addPicture(picture);
		
		picture = provider.getPicture(picture.getName());
		picture.setName(picture.getName() + "updated");
		provider.updatePicture(picture);
	}
}
