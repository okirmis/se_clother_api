package de.ovgu.cse.se.ClotherAPI.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

//@JsonIdentityInfo(scope=Picture.class, generator = ObjectIdGenerators.PropertyGenerator.class, property = "PICTURE_ID")
public class Tag implements IEntity {
	@JsonProperty("TAG_ID")
	public Long id;
	@JsonProperty("NAME")
    private String name;
	@JsonProperty("TYPE_ID")
	private Type type;
	@JsonProperty("USER_ID")
	private User creator;

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public User getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
