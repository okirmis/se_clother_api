package de.ovgu.cse.se.ClotherAPI.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(scope=Occasion.class, generator = ObjectIdGenerators.PropertyGenerator.class, property = "OCCASION_ID")
public class Occasion implements IEntity {

	@JsonProperty("OCCASION_ID")
	private Long id;
	@JsonProperty("NAME")
    private String name;

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
