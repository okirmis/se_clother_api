package de.ovgu.cse.se.ClotherAPI.database;

import com.dreamfactory.client.ApiException;

import de.ovgu.cse.se.ClotherAPI.models.Picture;
import de.ovgu.cse.se.ClotherAPI.models.Picture;

import java.util.List;

public class PictureProvider {
    public final String tableName = "pictures";

	public PictureProvider() {

	}

	public Picture addPicture(Picture picture) throws ApiException {
        return SessionProvider.getSession().addModel(picture, tableName);
    }

	public Picture deletePicture(Picture picture) throws ApiException {
        return SessionProvider.getSession().delModel(picture, tableName, picture.getId().toString());
    }

	public Picture updatePicture(Picture picture) throws ApiException {
        return SessionProvider.getSession().updModel(picture, tableName, picture.getId().toString());
    }

	public Picture getPicture(Long pictureId) throws ApiException {
        return SessionProvider.getSession().getModel(Picture.class, tableName, pictureId.toString());
    }
	
	public Picture getPicture(String pictureName) throws ApiException {
		assert ! pictureName.contains("'");
		String filter = String.format("NAME = '%s'", pictureName);
        List<Picture> pictures = SessionProvider.getSession().findModel(Picture.class, tableName, filter);
        if (pictures.size() == 1) {
			return pictures.get(0);
		} else {
			throw new ApiException(417, "ZeroOrManyFound");
		}
	}


    public List<Picture> getPictures() throws ApiException {
        return SessionProvider.getSession().listModel(Picture.class, tableName);
    }

}
