package de.ovgu.cse.se.ClotherAPI.database;

import com.dreamfactory.client.ApiException;
import de.ovgu.cse.se.ClotherAPI.models.Occasion;

import java.util.List;

public class OccasionProvider {
    public final String tableName = "occasions";

	public OccasionProvider() {

	}

	public Occasion addOccasion(Occasion occasion) throws ApiException {
        return SessionProvider.getSession().addModel(occasion, tableName);
    }

	public Occasion deleteOccasion(Occasion occasion) throws ApiException {
        return SessionProvider.getSession().delModel(occasion, tableName, occasion.getId().toString());
    }

	public Occasion updateOccasion(Occasion occasion) throws ApiException {
        return SessionProvider.getSession().updModel(occasion, tableName, occasion.getId().toString());
    }

	public Occasion getOccasion(Long occasionId) throws ApiException {
        return SessionProvider.getSession().getModel(Occasion.class, tableName, occasionId.toString());
    }

	public Occasion getOccasion(String occasionName) throws ApiException {
		assert ! occasionName.contains("'");
		String filter = String.format("NAME = '%s'", occasionName);
        List<Occasion> occasions = SessionProvider.getSession().findModel(Occasion.class, tableName, filter);
        if (occasions.size() == 1) {
			return occasions.get(0);
		} else {
			throw new ApiException(417, "ZeroOrManyFound");
		}
	}

	public List<Occasion> getOccasions() throws ApiException {
        return SessionProvider.getSession().listModel(Occasion.class, tableName);
    }

}
