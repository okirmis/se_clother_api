package de.ovgu.cse.se.ClotherAPI.exceptions;

import com.dreamfactory.client.ApiException;

/**
 * Created by Severin Orth on 18.05.15.
 * This file is part of the ClotherAPI project
 */
public class IntegrityConstraintViolation extends ApiException {
    private final String caption;


    public IntegrityConstraintViolation(String caption, String message) {
        super();

        this.caption = caption;

        this.setMessage(message);
    }


    @Override public String getMessage() {
        return this.caption + System.lineSeparator() + super.getMessage();
    }
}
