package de.ovgu.cse.se.ClotherAPI;

import java.util.Date;

import de.ovgu.cse.se.ClotherAPI.exceptions.IllegalObjectProviderContextException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotAddedException;
import de.ovgu.cse.se.ClotherAPI.models.Gender;
import de.ovgu.cse.se.ClotherAPI.models.User;

public class Main {

	public static User generateNewUser(String email) {
		User user = new User();
		user.setEmail(email);
		user.setFirstname("Max");
		user.setLastname("Mustermann");
		user.setBirthdate(new Date(12345));
		user.setGender(Gender.MALE);
		user.setPassword("password");

		return user;
	}
	
	public static void main(String[] args) throws UserNotAddedException, IllegalObjectProviderContextException {
        IObjectProvider provider = ObjectProviderFactory.getObjectProvider(ConfigurationContext.MOCKUP);
        User user = generateNewUser("testuser900@ovgu.de");
        provider.addUser(user);
        System.out.print("fertig");
	}

}
