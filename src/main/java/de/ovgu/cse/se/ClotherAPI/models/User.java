package de.ovgu.cse.se.ClotherAPI.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import de.ovgu.cse.se.ClotherAPI.helper.PasswordConverter;

@JsonIdentityInfo(scope=User.class, generator = ObjectIdGenerators.PropertyGenerator.class, property = "USER_ID")
public class User implements IEntity {
	@JsonProperty("USER_ID")
	private Long id;
	@JsonProperty("FIRSTNAME")
	private String firstname;
	@JsonProperty("LASTNAME")
	private String lastname;
	@JsonProperty("CREATION_TIME")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date creationTime;
	@JsonProperty("LAST_LOGIN")
	private Date lastLoginTime;
	@JsonProperty("EMAIL")
	private String email;
	@JsonProperty("GENDER")
	private Gender gender;
	@JsonProperty("BIRTHDATE")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date birthdate;
	@JsonProperty("CREDITSCORE")
	private int creditscore;
	@JsonProperty("PASSWORD")
	private String password;
	@JsonManagedReference(value="picture-user")
	private List<Picture> pictures;
	@JsonManagedReference(value="vote-user")
	private List<Vote> votes;

	
	public Long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getFirstname() {
		return firstname;
	}


	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}


	public String getLastname() {
		return lastname;
	}


	public void setLastname(String lastname) {
		this.lastname = lastname;
	}


	public Date getCreationTime() {
		return creationTime;
	}


	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public Gender getGender() {
		return gender;
	}


	public void setGender(Gender gender) {
		this.gender = gender;
	}


	public Date getBirthdate() {
		return birthdate;
	}


	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}


	public int getCreditscore() {
		return creditscore;
	}


	public void setCreditscore(int creditscore) {
		this.creditscore = creditscore;
	}


	public String getPassword() {
		return password;
	}

    public void setPasswordPlainText( String password ) {
		this.password = new PasswordConverter().convertToSHA(password);
    }

	public void setPassword( String password ) {
		this.password = password;
	}


	public List<Picture> getPicture() {
		return pictures;
	}


	public void addPicture(Picture picture) {
		if (this.pictures == null) {
			this.pictures = new ArrayList<>();
		}
		this.pictures.add(picture);
	}


	public void removePicture(Picture picture) {
		this.pictures.remove(picture);
	}


	public List<Vote> getVotes() {
		return votes;
	}


	public void setVotes(List<Vote> votes) {
		this.votes = votes;
	}


	public void addVote(Vote vote) {
		if (this.votes == null) {
			this.votes = new ArrayList<>();
		}
		this.votes.add(vote);
	}


	public void removeVote(Vote vote) {
		this.votes.remove(vote);
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((birthdate == null) ? 0 : birthdate.hashCode());
		result = prime * result
				+ ((creationTime == null) ? 0 : creationTime.hashCode());
		result = prime * result + creditscore;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result
				+ ((firstname == null) ? 0 : firstname.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result
				+ ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (creationTime == null) {
			if (other.creationTime != null)
				return false;
		} else if (!creationTime.equals(other.creationTime))
			return false;
		if (creditscore != other.creditscore)
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstname == null) {
			if (other.firstname != null)
				return false;
		} else if (!firstname.equals(other.firstname))
			return false;
		if (gender == null) {
			if (other.gender != null)
				return false;
		} else if (!gender.equals(other.gender))
			return false;
		if (id != other.id)
			return false;
		if (lastname == null) {
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		return true;
	}


	public void setCreationtime(Date date) {
		this.creationTime = date;

	}


	public void setBirthday(Date date) {
		this.birthdate = date;

	}


	public List<Picture> getPictures() {
		return pictures;
	}


	public void setPictures(List<Picture> pictures) {
		this.pictures = pictures;
	}


	public Date getLastLoginTime() {
		return lastLoginTime;
	}


	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

}
