package de.ovgu.cse.se.ClotherAPI;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import com.dreamfactory.client.ApiException;

import de.ovgu.cse.se.ClotherAPI.database.OccasionProvider;
import de.ovgu.cse.se.ClotherAPI.database.PictureProvider;
import de.ovgu.cse.se.ClotherAPI.database.SessionProvider;
import de.ovgu.cse.se.ClotherAPI.database.TagProvider;
import de.ovgu.cse.se.ClotherAPI.database.TypeProvider;
import de.ovgu.cse.se.ClotherAPI.database.UserProvider;
import de.ovgu.cse.se.ClotherAPI.database.VoteProvider;
import de.ovgu.cse.se.ClotherAPI.exceptions.IllegalObjectProviderContextException;
import de.ovgu.cse.se.ClotherAPI.exceptions.OccasionNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.OccasionNotFoundException;
import de.ovgu.cse.se.ClotherAPI.exceptions.OccasionNotUpdatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.PictureNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.PictureNotDeletedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.PictureNotFoundException;
import de.ovgu.cse.se.ClotherAPI.exceptions.PictureNotUpdatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.TagNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.TagNotFoundException;
import de.ovgu.cse.se.ClotherAPI.exceptions.TagNotUpdatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.TypeNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.TypeNotFoundException;
import de.ovgu.cse.se.ClotherAPI.exceptions.TypeNotUpdatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotAuthenticatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotDeletedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotUpdatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserdataNotCorrectException;
import de.ovgu.cse.se.ClotherAPI.exceptions.VersionCouldNotbeResolvedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.VoteNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.VoteNotDeletedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.VoteNotFoundException;
import de.ovgu.cse.se.ClotherAPI.exceptions.VoteNotUpdatedException;
import de.ovgu.cse.se.ClotherAPI.helper.IVersionHelper;
import de.ovgu.cse.se.ClotherAPI.helper.PasswordConverter;
import de.ovgu.cse.se.ClotherAPI.helper.VersionHelper;
import de.ovgu.cse.se.ClotherAPI.models.Occasion;
import de.ovgu.cse.se.ClotherAPI.models.Picture;
import de.ovgu.cse.se.ClotherAPI.models.Tag;
import de.ovgu.cse.se.ClotherAPI.models.Type;
import de.ovgu.cse.se.ClotherAPI.models.User;
import de.ovgu.cse.se.ClotherAPI.models.Vote;

/**
 * @author lipaczew
 * 
 */
public class ObjectProvider implements IObjectProvider {

	private User user = null;

	public ObjectProvider() throws IllegalObjectProviderContextException {
		this(ConfigurationContext.PRODUCTION);
	}

	public ObjectProvider(ConfigurationContext context)
			throws IllegalObjectProviderContextException {

		// check for API Version
		IVersionHelper versionHelper = new VersionHelper();
		try {
			if (!versionHelper.isApiUpToDate())
				Logger.getLogger("de.ovgu.cse.se.clotherAPI")
						.warning(
								"Your Clother API Version ("
										+ versionHelper.getLocalVersion()
										+ ") is not up-to-date. Please download and deploy the newest version ("
										+ versionHelper.getOnlineVersion()
										+ ").");
		} catch (VersionCouldNotbeResolvedException e) {
			Logger.getLogger("de.ovgu.cse.se.clotherAPI")
					.warning(
							"Could not retreive the online version number. Probably you do not have an active internet connection. However the program might still work, but make sure to check that there is no new version available!");
		} catch (Exception e) {
			e.printStackTrace();
		}

		// create new Session
		try {
			SessionProvider.createSession(context);
		} catch (ApiException e) {
			Logger.getLogger("de.ovgu.cse.se.clotherAPI").warning(
					"Session could not be created!");
		}
	}

	public void addOccasion(Occasion occasion)
			throws OccasionNotAddedException, UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		try {
			new OccasionProvider().addOccasion(occasion);
		} catch (ApiException e) {
			e.printStackTrace();
			throw new OccasionNotAddedException();
		}

	}

	/**
	 * speichert ein Bild in der Datenbank ab.
	 * 
	 * @param picture
	 *            Bild das hochgeladen werden soll
	 * @throws PictureNotAddedException
	 * @throws UserNotAuthenticatedException
	 */
	public void addPicture(Picture picture) throws PictureNotAddedException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		try {
			picture.setCreator(user);
			new PictureProvider().addPicture(picture);
		} catch (ApiException e) {
			e.printStackTrace();
			throw new PictureNotAddedException();
		}
	}

	public void addTag(Tag tag) throws TagNotAddedException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		try {
			new TagProvider().addTag(tag);
		} catch (ApiException e) {
			e.printStackTrace();
			throw new TagNotAddedException();
		}
	}

	public void addType(Type type) throws TypeNotAddedException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		try {
			new TypeProvider().addType(type);
		} catch (ApiException e) {
			e.printStackTrace();
			throw new TypeNotAddedException();
		}
	}

	/**
	 * legt einen neuen User in der Datenbank an
	 * 
	 * @param user
	 *            Userobjekt, welches in der Datenbank gespeichert werden soll
	 * @throws UserNotAddedException
	 */
	public void addUser(User user) throws UserNotAddedException {
		try {
			user.setCreationtime(new Date());
			new UserProvider().addUser(user);
		} catch (ApiException e) {
			e.printStackTrace();
			throw new UserNotAddedException();
		}
	}

	/**
	 * Adds a vote to the database.
	 * 
	 * @param vote
	 *            the vote that should be added to the picture
	 * @throws VoteNotAddedException
	 */
	public void addVote(Vote vote) throws VoteNotAddedException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		try {
			vote.setCreator(user);
			new VoteProvider().addVote(vote);
		} catch (ApiException e) {
			e.printStackTrace();
			throw new VoteNotAddedException();
		}

	}

	/**
	 * überprüft, ob die angegebenen Benutzerdaten korrekt sind. Wenn ja, wir
	 * der Benutzer zurückgegeben. Ansonsten eine Exception
	 * 
	 * @param email
	 *            email address of the user
	 * @param password
	 *            user password which will be encrypted and checked against the
	 *            encrypted version in the database
	 * @return the user object of the authenticated user
	 * @throws UserdataNotCorrectException
	 */
	public User authenticate(String email, String password)
			throws UserdataNotCorrectException {
		try {
			user = new UserProvider().getUser(email, new PasswordConverter().convertToSHA(password));
			//user.setLastLoginTime(new Date());
			updateUser();
			return user;
		} catch (ApiException e) {
			e.printStackTrace();
			throw new UserdataNotCorrectException();
		} catch (UserNotUpdatedException e) {
			e.printStackTrace();
			throw new UserdataNotCorrectException();
		} catch (UserNotAuthenticatedException e) {
			//cannot happen at this point, as updateUser is executed when authentication was successful and user was set.
			return null;
		}
	}

	public void closeConnection() {
		user = null;
	}

	/**
	 * löscht das Entsprechende Bild aus der Datenbank
	 * 
	 * @param picture
	 *            Bild das gelöscht werden soll
	 * @throws PictureNotDeletedException
	 * @throws UserNotAuthenticatedException
	 */

	public void deletePicture(Picture picture)
			throws PictureNotDeletedException, UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		try {
			new PictureProvider().deletePicture(picture);
		} catch (ApiException e) {
			e.printStackTrace();
			throw new PictureNotDeletedException();
		}
	}

	/**
	 * löscht den authentifizierten User aus der Datenbank
	 * 
	 * @return true, wenn Account erfolgreich gelöscht wurde
	 * @throws UserNotAuthenticatedException
	 */
	public void deleteUser() throws UserNotDeletedException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		try {
			new UserProvider().deleteUser(user);
			user = null;
		} catch (ApiException e) {
			e.printStackTrace();
			throw new UserNotDeletedException();
		}
	}

	public void deleteVote(Vote vote) throws VoteNotDeletedException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		try {
			new VoteProvider().deleteVote(vote);
		} catch (Exception e) {
			e.printStackTrace();
			throw new VoteNotDeletedException();
		}
	}

	public Occasion getOccasion(String occasionname)
			throws OccasionNotFoundException, UserNotAuthenticatedException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		try {
			return new OccasionProvider().getOccasion(occasionname);
		} catch (Exception e) {
			e.printStackTrace();
			throw new OccasionNotFoundException();
		}
	}

	public List<Occasion> getOccasions() throws OccasionNotFoundException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		try {
			return new OccasionProvider().getOccasions();
		} catch (ApiException e) {
			e.printStackTrace();
			throw new OccasionNotFoundException();
		}
	}

	/**
	 * Deprecated, as own votes can be directly accessed using the user object
	 * and the associated votes list.
	 * 
	 * @return
	 * @throws VoteNotFoundException
	 */
	@Deprecated
	public List<Vote> getOwnVotes() throws VoteNotFoundException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		try {
			return new VoteProvider().getOwnVotes(user);
		} catch (ApiException e) {
			throw new VoteNotFoundException();
		}

	}

	public Picture getPicture(Long pictureid) throws PictureNotFoundException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		try {
			return new PictureProvider().getPicture(pictureid);
		} catch (ApiException e) {
			e.printStackTrace();
			throw new PictureNotFoundException();
		}
	}

	public Picture getPicture(String picturename)
			throws PictureNotFoundException, UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		try {
			return new PictureProvider().getPicture(picturename);
		} catch (ApiException e) {
			e.printStackTrace();
			throw new PictureNotFoundException();
		}
	}

	public List<Picture> getPictures() throws PictureNotFoundException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		try {
			return new PictureProvider().getPictures();
		} catch (ApiException e) {
			e.printStackTrace();
			throw new PictureNotFoundException();
		}
	}

	public Tag getTag(String tagname) throws TagNotFoundException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		try {
			return new TagProvider().getTag(tagname);
		} catch (ApiException e) {
			e.printStackTrace();
			throw new TagNotFoundException();
		}

	}

	public List<Tag> getTags() throws TagNotFoundException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		try {
			return new TagProvider().getTags();
		} catch (ApiException e) {
			e.printStackTrace();
			throw new TagNotFoundException();
		}

	}

	public Type getType(String typename) throws TypeNotFoundException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		try {
			return new TypeProvider().getType(typename);
		} catch (ApiException e) {
			e.printStackTrace();
			throw new TypeNotFoundException();
		}

	}

	public List<Type> getTypes() throws TypeNotFoundException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		try {
			return new TypeProvider().getTypes();
		} catch (ApiException e) {
			e.printStackTrace();
			throw new TypeNotFoundException();
		}
	}

	public User getUser() {
		return user;
	}

	public Vote getVote(Long voteid) throws VoteNotFoundException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		try {
			return new VoteProvider().getVote(voteid);
		} catch (ApiException e) {
			e.printStackTrace();
			throw new VoteNotFoundException();
		}

	}

	public void updateOccasion(Occasion occasion)
			throws OccasionNotUpdatedException, UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		try {
			new OccasionProvider().updateOccasion(occasion);
		} catch (Exception e) {
			e.printStackTrace();
			throw new OccasionNotUpdatedException();
		}

	}

	public void updatePicture(Picture picture)
			throws PictureNotUpdatedException, UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		try {
			picture.setCreator(user);
			new PictureProvider().updatePicture(picture);
		} catch (Exception e) {
			e.printStackTrace();
			throw new PictureNotUpdatedException();
		}
	}

	public void updateTag(Tag tag) throws TagNotUpdatedException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		try {
			new TagProvider().updateTag(tag);
		} catch (Exception e) {
			e.printStackTrace();
			throw new TagNotUpdatedException();
		}

	}

	public void updateType(Type type) throws TypeNotUpdatedException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		try {
			new TypeProvider().updateType(type);
		} catch (Exception e) {
			e.printStackTrace();
			throw new TypeNotUpdatedException();
		}

	}

	/**
	 * speichert Änderungen am authentifizierten User in der Datenbank
	 * 
	 * @return true, wenn Account erfolgreich geändert wurde
	 * @throws UserNotAuthenticatedException
	 */
	public void updateUser() throws UserNotUpdatedException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		try {
			new UserProvider().updateUser(user);
		} catch (ApiException e) {
			e.printStackTrace();
			throw new UserNotUpdatedException();
		}
	}

	public void updateVote(Vote vote) throws VoteNotUpdatedException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		try {
			vote.setCreator(user);
			new VoteProvider().updateVote(vote);
		} catch (ApiException e) {
			e.printStackTrace();
			throw new VoteNotUpdatedException();
		}

	}
}
