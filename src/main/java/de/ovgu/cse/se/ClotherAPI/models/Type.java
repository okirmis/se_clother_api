package de.ovgu.cse.se.ClotherAPI.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(scope=Picture.class, generator = ObjectIdGenerators.PropertyGenerator.class, property = "TYPE_ID")
public class Type implements IEntity {

	@JsonProperty("TYPE_ID")
	public Long id;
	@JsonProperty("NAME")
    public String name;

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
