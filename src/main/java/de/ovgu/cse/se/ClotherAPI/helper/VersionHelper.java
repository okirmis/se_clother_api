package de.ovgu.cse.se.ClotherAPI.helper;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import de.ovgu.cse.se.ClotherAPI.exceptions.VersionCouldNotbeResolvedException;

public class VersionHelper implements IVersionHelper {

	public String getApiVersion(InputStream stream)
			throws VersionCouldNotbeResolvedException {
		String result = null;

		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document document = db.parse(new InputSource(stream));

			// get the content of the file pom.xml at revision tip
			XPathFactory xpf = XPathFactory.newInstance();
			XPath xp = xpf.newXPath();
			result = xp.evaluate("/project/version[1]/text()",
					document.getDocumentElement());
		} catch (XPathExpressionException | FactoryConfigurationError
				| ParserConfigurationException | SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;

	}

	public boolean isApiUpToDate() throws VersionCouldNotbeResolvedException {
		try {
			return this.getOnlineVersion().equals(this.getLocalVersion());
		} catch (IOException e) {
			e.printStackTrace();
			throw new VersionCouldNotbeResolvedException();
		}
	}

	public String getOnlineVersion() throws VersionCouldNotbeResolvedException,
			IOException {
		return this
				.getApiVersion(new URL(
						"https://bitbucket.org/api/1.0/repositories/cse_admin/se_clother_api/raw/HEAD/pom.xml")
						.openStream());
	}

	public String getLocalVersion() throws FileNotFoundException, VersionCouldNotbeResolvedException {
		return this
				.getApiVersion(this.getClass().getResourceAsStream("/clotherapi.xml"));
	}
}
