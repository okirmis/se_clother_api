package de.ovgu.cse.se.ClotherAPI.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(scope=Picture.class, generator = ObjectIdGenerators.PropertyGenerator.class, property = "PICTURE_ID")
public class Picture implements IEntity {
	@JsonProperty("PICTURE_ID")
	private Long id;
	//@JsonProperty("USER_ID")
	@JsonBackReference(value="picture-user")
	private User creator;
	@JsonProperty("NAME")
	private String name;
	@JsonProperty("OCCASION_ID")
	//@JsonManagedReference(value="picture-occasion")
	private Occasion occasion;
	@JsonProperty("IMAGE")
	private byte[] image;
	@JsonProperty("CREATION_TIME")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date creationTime;
	@JsonProperty("TAG_ID")
	private List<Tag> tags;
	@JsonManagedReference(value="vote-picture")
	private List<Vote> votes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Occasion getOccasion() {
		return occasion;
	}

	public void setOccasion(Occasion occasion) {
		this.occasion = occasion;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public void addTag(Tag tag) {
		if (this.tags == null)
			this.tags = new ArrayList<>();
		this.tags.add(tag);
	}

	public void removeTag(Tag tag) {
		this.tags.remove(tag);
	}

	public List<Vote> getVotes() {
		return votes;
	}

	public void setVotes(List<Vote> votes) {
		this.votes = votes;
	}

	public void addVote(Vote vote) {
		if (this.votes == null)
			this.votes = new ArrayList<>();
		this.votes.add(vote);
	}

	public void removeVote(Vote vote) {
		this.votes.remove(vote);
	}
}
