package de.ovgu.cse.se.ClotherAPI;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import de.ovgu.cse.se.ClotherAPI.exceptions.IllegalObjectProviderContextException;
import de.ovgu.cse.se.ClotherAPI.exceptions.OccasionNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.OccasionNotFoundException;
import de.ovgu.cse.se.ClotherAPI.exceptions.OccasionNotUpdatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.PictureNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.PictureNotDeletedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.PictureNotFoundException;
import de.ovgu.cse.se.ClotherAPI.exceptions.PictureNotUpdatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.TagNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.TagNotFoundException;
import de.ovgu.cse.se.ClotherAPI.exceptions.TagNotUpdatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.TypeNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.TypeNotFoundException;
import de.ovgu.cse.se.ClotherAPI.exceptions.TypeNotUpdatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotAuthenticatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotDeletedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserNotUpdatedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.UserdataNotCorrectException;
import de.ovgu.cse.se.ClotherAPI.exceptions.VersionCouldNotbeResolvedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.VoteNotAddedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.VoteNotDeletedException;
import de.ovgu.cse.se.ClotherAPI.exceptions.VoteNotFoundException;
import de.ovgu.cse.se.ClotherAPI.exceptions.VoteNotUpdatedException;
import de.ovgu.cse.se.ClotherAPI.helper.EntityGenerator;
import de.ovgu.cse.se.ClotherAPI.helper.IVersionHelper;
import de.ovgu.cse.se.ClotherAPI.helper.VersionHelper;
import de.ovgu.cse.se.ClotherAPI.models.Occasion;
import de.ovgu.cse.se.ClotherAPI.models.Picture;
import de.ovgu.cse.se.ClotherAPI.models.Tag;
import de.ovgu.cse.se.ClotherAPI.models.Type;
import de.ovgu.cse.se.ClotherAPI.models.User;
import de.ovgu.cse.se.ClotherAPI.models.Vote;

/**
 * @author lipaczew
 * 
 */
public class ObjectProviderMockUp implements IObjectProvider {

	private User user = null;
	private List<Occasion> occasions;
	private List<Picture> pictures;
	private List<User> users;
	private List<Type> types;
	private List<Tag> tags;
	private List<Vote> votes;

	public ObjectProviderMockUp() throws IllegalObjectProviderContextException {
		this(ConfigurationContext.MOCKUP);
	}

	public ObjectProviderMockUp(ConfigurationContext context)
			throws IllegalObjectProviderContextException {

		// check for API Version
		IVersionHelper versionHelper = new VersionHelper();
		try {
			if (!versionHelper.isApiUpToDate())
				Logger.getLogger("de.ovgu.cse.se.clotherAPI")
						.warning(
								"Your Clother API Version ("
										+ versionHelper.getLocalVersion()
										+ ") is not up-to-date. Please download and deploy the newest version ("
										+ versionHelper.getOnlineVersion()
										+ ").");
		} catch (VersionCouldNotbeResolvedException e) {
			Logger.getLogger("de.ovgu.cse.se.clotherAPI")
					.warning(
							"Could not retreive the online version number. Probably you do not have an active internet connection. However the program might still work, but make sure to check that there is no new version available!");
		} catch (Exception e) {
			e.printStackTrace();
		}

		// do not create a Session in MockUpMode
		occasions = new ArrayList<>();
		pictures = new ArrayList<>();
		users = new ArrayList<>();
		types = new ArrayList<>();
		tags = new ArrayList<>();
		votes = new ArrayList<>();

		try {

			EntityGenerator gen = new EntityGenerator();
			for (int i = 0; i < 5; i++) {
				User user = gen.generateNewUser();
				user.setId(i);

				addUser(user);
			}
			for (int i = 0; i < 10; i++) {
				Picture pic = gen.generateNewPicture();
				pic.setId(Long.valueOf(i));
				addPicture(pic);
				User user = gen.getRandomUser();
				pic.setCreator(user);
				user.addPicture(pic);
			}
			for (int i = 0; i < 20; i++) {
				Vote vote = gen.generateNewVote();
				vote.setId(Long.valueOf(i));
				addVote(vote);
				User user = gen.getRandomUser();
				user.addVote(vote);
				vote.setCreator(user);
				Picture picture = gen.getRandomPicture();
				vote.setPicture(picture);
				picture.addVote(vote);
			}
		} catch (UserNotAddedException e) {
			// TODO Auto-generated catch block
		} catch (PictureNotAddedException e) {
			// TODO Auto-generated catch block
		} catch (UserNotAuthenticatedException e) {
			// TODO Auto-generated catch block
		} catch (VoteNotAddedException e) {
			// TODO Auto-generated catch block
		}

	}

	public void addOccasion(Occasion occasion)
			throws OccasionNotAddedException, UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		occasions.add(occasion);

	}

	/**
	 * speichert ein Bild in der Datenbank ab.
	 * 
	 * @param picture
	 *            Bild das hochgeladen werden soll
	 * @throws PictureNotAddedException
	 * @throws UserNotAuthenticatedException
	 */
	public void addPicture(Picture picture) throws PictureNotAddedException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		pictures.add(picture);
	}

	public void addTag(Tag tag) throws TagNotAddedException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		tags.add(tag);
	}

	public void addType(Type type) throws TypeNotAddedException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		types.add(type);
	}

	/**
	 * legt einen neuen User in der Datenbank an
	 * 
	 * @param user
	 *            Userobjekt, welches in der Datenbank gespeichert werden soll
	 * @throws UserNotAddedException
	 */
	public void addUser(User user) throws UserNotAddedException {
		users.add(user);
	}

	/**
	 * Adds a vote to the database.
	 * 
	 * @param vote
	 *            the vote that should be added to the picture
	 * @throws VoteNotAddedException
	 */
	public void addVote(Vote vote) throws VoteNotAddedException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		votes.add(vote);

	}

	/**
	 * überprüft, ob die angegebenen Benutzerdaten korrekt sind. Wenn ja, wir
	 * der Benutzer zurückgegeben. Ansonsten eine Exception
	 * 
	 * @param email
	 *            email address of the user
	 * @param password
	 *            user password which will be encrypted and checked against the
	 *            encrypted version in the database
	 * @return the user object of the authenticated user
	 * @throws UserdataNotCorrectException
	 */
	public User authenticate(String email, String password)
			throws UserdataNotCorrectException {
		user = new EntityGenerator().generateNewUser();
		return user;
	}

	public void closeConnection() {
		user = null;
	}

	/**
	 * löscht das Entsprechende Bild aus der Datenbank
	 * 
	 * @param picture
	 *            Bild das gelöscht werden soll
	 * @throws PictureNotDeletedException
	 * @throws UserNotAuthenticatedException
	 */

	public void deletePicture(Picture picture)
			throws PictureNotDeletedException, UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		pictures.remove(picture);
	}

	/**
	 * löscht den authentifizierten User aus der Datenbank
	 * 
	 * @return true, wenn Account erfolgreich gelöscht wurde
	 * @throws UserNotAuthenticatedException
	 */
	public void deleteUser() throws UserNotDeletedException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		users.remove(user);
	}

	public void deleteVote(Vote vote) throws VoteNotDeletedException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		votes.remove(vote);
	}

	public Occasion getOccasion(String occasionname)
			throws OccasionNotFoundException, UserNotAuthenticatedException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		Iterator<Occasion> it = occasions.listIterator();
		while (it.hasNext()) {
			Occasion el = it.next();
			if (el.getName().equals(occasionname))
				return el;
		}
		throw new OccasionNotFoundException();
	}

	public List<Occasion> getOccasions() throws OccasionNotFoundException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		return occasions;
	}

	/**
	 * Deprecated, as own votes can be directly accessed using the user object
	 * and the associated votes list.
	 * 
	 * @return
	 * @throws VoteNotFoundException
	 */
	@Deprecated
	public List<Vote> getOwnVotes() throws VoteNotFoundException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		List<Vote> returnList = new ArrayList<>();
		Iterator<Vote> it = votes.listIterator();
		while (it.hasNext()) {
			Vote el = it.next();
			if (el.getCreator().equals(user))
				returnList.add(el);
		}
		if (returnList.size() > 0)
			return returnList;
		throw new VoteNotFoundException();

	}

	public Picture getPicture(Long pictureid) throws PictureNotFoundException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		Iterator<Picture> it = pictures.listIterator();
		while (it.hasNext()) {
			Picture el = it.next();
			if (el.getId().equals(pictureid))
				return el;
		}
		throw new PictureNotFoundException();
	}

	public Picture getPicture(String picturename)
			throws PictureNotFoundException, UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		Iterator<Picture> it = pictures.listIterator();
		while (it.hasNext()) {
			Picture el = it.next();
			if (el.getName().equals(picturename))
				return el;
		}
		throw new PictureNotFoundException();
	}

	public List<Picture> getPictures() throws PictureNotFoundException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		return pictures;
	}

	public Tag getTag(String tagname) throws TagNotFoundException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		Iterator<Tag> it = tags.listIterator();
		while (it.hasNext()) {
			Tag el = it.next();
			if (el.getName().equals(tagname))
				return el;
		}
		throw new TagNotFoundException();

	}

	public List<Tag> getTags() throws TagNotFoundException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		return tags;

	}

	public Type getType(String typename) throws TypeNotFoundException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		Iterator<Type> it = types.listIterator();
		while (it.hasNext()) {
			Type el = it.next();
			if (el.getName().equals(typename))
				return el;
		}
		throw new TypeNotFoundException();

	}

	public List<Type> getTypes() throws TypeNotFoundException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		return types;
	}

	public User getUser() {
		return user;
	}

	public Vote getVote(Long voteid) throws VoteNotFoundException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		Iterator<Vote> it = votes.listIterator();
		while (it.hasNext()) {
			Vote el = it.next();
			if (el.getId().equals(voteid))
				return el;
		}
		throw new VoteNotFoundException();
	}

	public void updateOccasion(Occasion occasion)
			throws OccasionNotUpdatedException, UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		// nothing to do here, as the Element is already in the list and updated
		// accordingly

	}

	public void updatePicture(Picture picture)
			throws PictureNotUpdatedException, UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		// nothing to do here, as the Element is already in the list and updated
		// accordingly
	}

	public void updateTag(Tag tag) throws TagNotUpdatedException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		// nothing to do here, as the Element is already in the list and updated
		// accordingly

	}

	public void updateType(Type type) throws TypeNotUpdatedException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		// nothing to do here, as the Element is already in the list and updated
		// accordingly

	}

	/**
	 * speichert Änderungen am authentifizierten User in der Datenbank
	 * 
	 * @return true, wenn Account erfolgreich geändert wurde
	 * @throws UserNotAuthenticatedException
	 */
	public void updateUser() throws UserNotUpdatedException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		// nothing to do here, as the Element is already in the list and updated
		// accordingly
	}

	public void updateVote(Vote vote) throws VoteNotUpdatedException,
			UserNotAuthenticatedException {
		if (user == null)
			throw new UserNotAuthenticatedException();
		// nothing to do here, as the Element is already in the list and updated
		// accordingly

	}
}
