package de.ovgu.cse.se.ClotherAPI.exceptions;

public class NotAddedException extends Exception {

	public NotAddedException() {
		super();
	}

	public NotAddedException(String message) {
		super(message);
	}

	public NotAddedException(Throwable cause) {
		super(cause);
	}

	public NotAddedException(String message, Throwable cause) {
		super(message, cause);
	}

	public NotAddedException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}