package de.ovgu.cse.se.ClotherAPI.exceptions;

public class NotUpdatedException extends Exception {

	public NotUpdatedException() {
		super();
	}

	public NotUpdatedException(String message) {
		super(message);
	}

	public NotUpdatedException(Throwable cause) {
		super(cause);
	}

	public NotUpdatedException(String message, Throwable cause) {
		super(message, cause);
	}

	public NotUpdatedException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}