package de.ovgu.cse.se.ClotherAPI.database;

import com.dreamfactory.client.ApiException;
import com.dreamfactory.client.ApiInvoker;
import com.dreamfactory.client.JsonUtil;
import com.dreamfactory.model.Session;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JavaType;
import de.ovgu.cse.se.ClotherAPI.ConfigurationContext;
import de.ovgu.cse.se.ClotherAPI.exceptions.IntegrityConstraintViolation;
import de.ovgu.cse.se.ClotherAPI.exceptions.UnknownColumnName;
import de.ovgu.cse.se.ClotherAPI.models.IEntity;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.*;

/**
 * Created by Severin Orth on 18.05.15.
 * This file is part of the ClotherAPI project
 */
public class SessionProvider {
    private static SessionProvider instance = null;

    /**
     * Le Provider Fields
     */
    private final RequestHeader        header;
    private final ConfigurationContext context;


    /**
     * Le Provider Constructor
     */
    private SessionProvider(RequestHeader header, ConfigurationContext context) {
        this.header = header;
        this.context = context;
    }


    /**
     * Session Instance
     * will be created of not present, otherwise last session is used
     *
     * @return Instance of Session Instance
     */
    public static SessionProvider createSession(ConfigurationContext context) throws ApiException {
        if (instance == null) {
            String urlPath = String.format("/rest/%s/%s", "user", "session");
            Map<String, String> urlParams = new HashMap<>();
            String urlPayload = context.getRole();
            RequestHeader requestHeader = new RequestHeader(context.getName());
            String response = ApiInvoker.getInstance()
                    .invokeAPI(context.getUrl(), urlPath, "POST", urlParams, urlPayload, requestHeader, "application/json");

            Session session = (Session) ApiInvoker.deserialize(response, "", Session.class);
            requestHeader.setSessionToken(session.getSession_id());
            requestHeader.raiseForSessionToken();
            instance = new SessionProvider(requestHeader, context);
        }
        return instance;
    }


    /**
     * Session Instance
     *
     * @return Instance of Session Instance
     */
    public static SessionProvider getSession() throws ApiException {
        if (instance == null) {
            throw new ApiException(403, "Session not created.");
        }
        return instance;
    }


    /**
     * Convert some exceptions to a more pretty printing
     */
    private static ApiException handleException(ApiException e) {
        String message;
        try {
            //noinspection unchecked
            message = (String) ((List<Map>) JsonUtil.getJsonMapper().readValue(e.getMessage(), Map.class).get("error"))
                    .get(0).get("message");
        } catch (IOException | ClassCastException ignored) {
            return e;
        }
        String[] strings = message.split("\n");
        if (strings.length != 2) {
            return e;
        }
        if (strings[1].contains("SQLSTATE")) {
            String[] args = strings[1].split(":");
            if (args.length > 2 && args[1].trim().equals("SQLSTATE[23000]")) {
                return new IntegrityConstraintViolation(args[2].trim(), args[3].trim());
            } else if (args.length > 2 && args[1].trim().equals("SQLSTATE[42S22]")) {
                return new UnknownColumnName(args[2].trim(), args[3].trim());
            } else {
                return e;
            }
        }

        return null;
    }


    /**
     * Get a Model from Session
     */
    @SuppressWarnings("unchecked") <T extends IEntity> T getModel(Class<T> model, String tableName, String id)
            throws ApiException {
        String urlPath = context.getUrlPath(tableName, id);
        Map<String, String> urlParams = new HashMap<>();
        urlParams.put("table_name", tableName);
        urlParams.put("id", id);
        urlParams.put("related", "*");
        String response = ApiInvoker.getInstance()
                .invokeAPI(context.getUrl(), urlPath, "GET", urlParams, "", header, "application/json");
        return (T) ApiInvoker.deserialize(response, "", model);
    }


    /**
     * Set a Model into Session
     */
    @SuppressWarnings("unchecked") <T extends IEntity> T addModel(T model, String tableName) throws ApiException {
        String urlPath = context.getUrlPath(tableName);
        Map<String, String> urlParams = new HashMap<>();
        urlParams.put("table_name", tableName);
        urlParams.put("related", "*");
        try {
            String response = ApiInvoker.getInstance()
                    .invokeAPI(context.getUrl(), urlPath, "POST", urlParams, model, header, "application/json");
            T entity = (T) ApiInvoker.deserialize(response, "", model.getClass());
            setSubscription(tableName, entity.getId());
            return entity;
        } catch (ApiException e) {
            throw handleException(e);
        }
    }


    /**
     * Delete a Model from Session
     */
    @SuppressWarnings("unchecked") <T extends IEntity> T delModel(T model, String tableName, String id)
            throws ApiException {
        String urlPath = context.getUrlPath(tableName, id);
        Map<String, String> urlParams = new HashMap<>();
        urlParams.put("table_name", tableName);
        urlParams.put("id", id);
        urlParams.put("related", "*");
        try {
            String response = ApiInvoker.getInstance()
                    .invokeAPI(context.getUrl(), urlPath, "DELETE", urlParams, model, header, "application/json");
            T entity = (T) ApiInvoker.deserialize(response, "", model.getClass());
            delSubscription(tableName, entity.getId());
            return entity;
        } catch (ApiException e) {
            throw handleException(e);
        }
    }


    /**
     * Update a Model in Session
     */
    @SuppressWarnings("unchecked") <T extends IEntity> T updModel(T model, String tableName, String id)
            throws ApiException {
        String urlPath = context.getUrlPath(tableName, id);
        Map<String, String> urlParams = new HashMap<>();
        urlParams.put("table_name", tableName);
        urlParams.put("id", id);
        urlParams.put("related", "*");
        try {
            String response = ApiInvoker.getInstance()
                    .invokeAPI(context.getUrl(), urlPath, "PATCH", urlParams, model, header, "application/json");
            T entity = (T) ApiInvoker.deserialize(response, "", model.getClass());
            setSubscription(tableName, entity.getId());
            return entity;
        } catch (ApiException e) {
            throw handleException(e);
        }
    }


    /**
     * Get a list of Model in Session
     */
    <T extends IEntity> List<T> listModel(Class<T> model, String tableName) throws ApiException {
        String ids = getSubscription(tableName).objectList;
        return listModel(model, tableName, ids);
    }


    /**
     * Get a list of Model in Session matching ids
     */
    @SuppressWarnings("unchecked") <T extends IEntity> List<T> listModel(Class<T> model, String tableName, String ids) throws ApiException {
        // Make request
        String urlPath = context.getUrlPath(tableName);
        Map<String, String> urlParams = new HashMap<>();
        urlParams.put("table_name", tableName);
        urlParams.put("ids", ids);
        urlParams.put("related", "*");
        String response = ApiInvoker.getInstance()
                .invokeAPI(context.getUrl(), urlPath, "GET", urlParams, model, header, "application/json");

        // Return Empty List on no results
        if (response.isEmpty()) {
            return new LinkedList<>();
        }

        // Parse Result
        response = response.substring(10, response.length() - 1);
        JavaType responseType = JsonUtil.getJsonMapper().getTypeFactory().constructCollectionType(List.class, model);
        try {
            return JsonUtil.getJsonMapper().readValue(response, responseType);
        } catch (IOException e) {
            throw new ApiException(500, e.getMessage());
        }
    }


    @SuppressWarnings("unchecked") <T extends IEntity> List<T> findModel(Class<T> model, String tableName, String filter) throws ApiException {
        // Make request
        String urlPath = context.getUrlPath(tableName);
        Map<String, String> urlParams = new HashMap<>();
        urlParams.put("table_name", tableName);
        urlParams.put("filter", URLEncoder.encode(filter));
        urlParams.put("related", "*");
        String response = ApiInvoker.getInstance()
                .invokeAPI(context.getUrl(), urlPath, "GET", urlParams, model, header, "application/json");

        // Return Empty List on no results
        if (response.isEmpty()) {
            return new LinkedList<>();
        }

        // Parse Result
        response = response.substring(10, response.length() - 1);
        JavaType responseType = JsonUtil.getJsonMapper().getTypeFactory().constructCollectionType(List.class, model);
        try {
            return JsonUtil.getJsonMapper().readValue(response, responseType);
        } catch (IOException e) {
            throw new ApiException(500, e.getMessage());
        }
    }


    private SubscribeModel getSubscription(String tableName) throws ApiException {
        String urlPath = context.getUrlPath("objects", tableName.toUpperCase());
        Map<String, String> urlParams = new HashMap<>();
        urlParams.put("table_name", "objects");
        String response = ApiInvoker.getInstance()
                .invokeAPI(context.getUrl(), urlPath, "GET", urlParams, SubscribeModel.class, header,
                        "application/json");
        return (SubscribeModel) ApiInvoker.deserialize(response, "", SubscribeModel.class);
    }


    private void setSubscription(String tableName, Long id) throws ApiException {
        SubscribeModel subscription = getSubscription(tableName);
        if (subscription.add(id.toString())) {
            String urlPath = context.getUrlPath("objects", tableName.toUpperCase());
            Map<String, String> urlParams = new HashMap<>();
            urlParams.put("table_name", "objects");
            ApiInvoker.getInstance().invokeAPI(context.getUrl(), urlPath, "PUT", urlParams, subscription, header, "application/json");
        }
    }


    private void delSubscription(String tableName, Long id) throws ApiException {
        SubscribeModel subscription = getSubscription(tableName);
        if (subscription.del(id.toString())) {
            String urlPath = context.getUrlPath("objects", tableName.toUpperCase());
            Map<String, String> urlParams = new HashMap<>();
            urlParams.put("table_name", "objects");
            ApiInvoker.getInstance().invokeAPI(context.getUrl(), urlPath, "PUT", urlParams, subscription, header, "application/json");
        }
    }

    /**
     *
     */
    private static class RequestHeader extends HashMap<String, String> {
        RequestHeader(String applicationName) {
            super();
            this.put("X-DreamFactory-Application-Name", applicationName);
            this.put("Accept", "application/json");
        }


        void setSessionToken(String sessionToken) {
            this.put("X-DreamFactory-Session-Token", sessionToken);
        }


        void raiseForSessionToken() throws NoSessionCreated {
            if (!this.containsKey("X-DreamFactory-Session-Token")) {
                throw new NoSessionCreated();
            }
        }
    }

    /**
     *
     */
    private static class NoSessionCreated extends ApiException {
    }

    /**
     *
     */
    private static class SubscribeModel {
        @JsonProperty("OBJECT_NAME") String objectName;
        @JsonProperty("OBJECT_LIST") String objectList;


        public boolean add(String string) {
            if (objectList == null) {
                objectList = "";
            }
            String[] values = objectList.split(",");
            if (!Arrays.asList(values).contains(string)) {
                objectList = "";
                for (String item : values) {
                    if (!objectList.isEmpty()) {
                        objectList += ",";
                    }
                    objectList += item;
                }
                if (!objectList.isEmpty()) {
                    objectList += ",";
                }
                objectList += string;
                return true;
            } else {
                return false;
            }
        }


        public boolean del(String string) {
            if (objectList == null) {
                objectList = "";
            }
            String[] values = objectList.split(",");
            Set<String> set = new HashSet<>(Arrays.asList(values));
            if (Arrays.asList(values).contains(string)) {
                objectList = "";
                for (String item : set) {
                    if (!objectList.isEmpty()) {
                        objectList += ",";
                    }
                    if (!item.equals(string))
                        objectList += item;
                }
                return true;
            } else {
                return false;
            }
        }
    }
}
