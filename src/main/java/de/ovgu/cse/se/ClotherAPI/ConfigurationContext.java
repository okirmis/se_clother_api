package de.ovgu.cse.se.ClotherAPI;

/**
 * Configuration for Object Provider
 *
 * You can subclass this enum if you want to provide our own configuration
 */
public enum ConfigurationContext {
	PRODUCTION("https://dsp-clother.cloud.dreamfactory.com", "/rest", "clotherAPI", "clotherdb", "test@example.com", "123456"),
	TEST("https://dsp-clother.cloud.dreamfactory.com", "/rest", "clotherAPI", "clotherdb_test", "test@example.com", "123456"),
	MOCKUP(null,null,null,null,null,null);

	final String dspUrl;
	final String dspSuffix;
	final String appName;
	final String schemaName;
	final String roleName;
	final String rolePassword;

	ConfigurationContext(String dspUrl, String dspSuffix, String appName, String schemaName, String roleName, String rolePassword) {
		this.dspUrl = dspUrl;
		this.dspSuffix = dspSuffix;
		this.appName = appName;
		this.schemaName = schemaName;
		this.roleName = roleName;
		this.rolePassword = rolePassword;
	}

	public String getUrl() {
		return this.dspUrl;
	}

	public String getName() {
		return this.appName;
	}

	public String getRole() {
		return String.format("{  \"email\": \"%s\", \"password\": \"%s\", \"duration\": 0}", roleName, rolePassword);
	}

	public String getUrlPath(String tableName) {
		return String.format("%s/%s/%s", dspSuffix, schemaName, tableName);
	}

	public String getUrlPath(String tableName, String entityId) {
		return String.format("%s/%s/%s/%s", dspSuffix, schemaName, tableName, entityId);
	}


}
