package de.ovgu.cse.se.ClotherAPI.database;

import com.dreamfactory.client.ApiException;
import de.ovgu.cse.se.ClotherAPI.models.Type;

import java.util.List;

public class TypeProvider {
    public final String tableName = "types";

	public TypeProvider() {

	}

	public Type addType(Type type) throws ApiException {
        return SessionProvider.getSession().addModel(type, tableName);
    }

	public Type deleteType(Type type) throws ApiException {
        return SessionProvider.getSession().delModel(type, tableName, type.getId().toString());
    }

	public Type updateType(Type type) throws ApiException {
        return SessionProvider.getSession().updModel(type, tableName, type.getId().toString());
    }

	public Type getType(Long typeId) throws ApiException {
        return SessionProvider.getSession().getModel(Type.class, tableName, typeId.toString());
    }

	public List<Type> getTypes() throws ApiException {
        return SessionProvider.getSession().listModel(Type.class, tableName);
    }

	public Type getType(String typeName) throws ApiException {
		assert ! typeName.contains("'");
		String filter = String.format("NAME = '%s'", typeName);
        List<Type> types = SessionProvider.getSession().findModel(Type.class, tableName, filter);
        if (types.size() == 1) {
			return types.get(0);
		} else {
			throw new ApiException(417, "ZeroOrManyFound");
		}
	}

}
