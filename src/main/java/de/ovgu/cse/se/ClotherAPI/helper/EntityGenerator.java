package de.ovgu.cse.se.ClotherAPI.helper;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import de.ovgu.cse.se.ClotherAPI.models.Gender;
import de.ovgu.cse.se.ClotherAPI.models.Occasion;
import de.ovgu.cse.se.ClotherAPI.models.Picture;
import de.ovgu.cse.se.ClotherAPI.models.Tag;
import de.ovgu.cse.se.ClotherAPI.models.Type;
import de.ovgu.cse.se.ClotherAPI.models.User;
import de.ovgu.cse.se.ClotherAPI.models.Vote;

public class EntityGenerator {

	private List<User> users;
	private List<Picture> pictures;
	private List<Vote> votes;
	private List<Tag> tags;
	private List<Type> types;
	private List<Occasion> occasions;

	public EntityGenerator() {
		users = new ArrayList<User>();
		pictures = new ArrayList<Picture>();
		votes = new ArrayList<Vote>();
		tags = new ArrayList<Tag>();
		types = new ArrayList<Type>();
		occasions = new ArrayList<Occasion>();
	}

	public User generateNewUser() {
		User user = new User();
		user.setEmail(users.size() + "@" + this.hashCode() + ".de");
		user.setFirstname("Max");
		user.setLastname("Mustermann");
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(0);
		cal.set(1980, 1, 1);
		user.setBirthdate(cal.getTime());
		user.setGender(Gender.MALE);
		user.setPassword("password");

		users.add(user);

		return user;
	}

	public Picture generateNewPicture() {
		Picture picture = new Picture();
		picture.setName("picture" + pictures.size() + this.hashCode());
		try {
			URL resourceUrl = getClass().getResource("/test.jpg");
			Path resourcePath = Paths.get(resourceUrl.toURI());
			picture.setImage(Files.readAllBytes(resourcePath));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		pictures.add(picture);
		return picture;
	}

	public Picture getLastPicture() {
		if (pictures.size() > 0)
			return pictures.get(pictures.size() - 1);
		return generateNewPicture();
	}

	public Tag generateNewTag() {
		Tag tag = new Tag();
		tag.setName("tag" + tags.size() + this.hashCode());
		tags.add(tag);
		return tag;
	}

	public Type generateNewType() {
		Type type = new Type();
		type.setName("type" + types.size() + this.hashCode());
		types.add(type);
		return type;
	}

	public Type getLastType() {
		if (types.size() > 0)
			return types.get(types.size() - 1);
		return generateNewType();
	}

	public Tag getLastTag() {
		if (tags.size() > 0)
			return tags.get(tags.size() - 1);
		return generateNewTag();
	}

	public Vote getLastVote() {
		if (votes.size() > 0)
			return votes.get(votes.size() - 1);
		return generateNewVote();
	}

	public Vote generateNewVote() {
		Vote vote = new Vote();
		vote.setComment("vote" + votes.size() + this.hashCode());
		votes.add(vote);
		return vote;
	}

	public Occasion getLastOccasion() {
		if (occasions.size() > 0)
			return occasions.get(occasions.size() - 1);
		return generateNewOccasion();
	}

	public Occasion generateNewOccasion() {
		Occasion occasion = new Occasion();
		occasion.setName("occasion" + occasions.size() + this.hashCode());
		occasions.add(occasion);
		return occasion;
	}

	public Tag getRandomTag() {
		int i = new Random().nextInt(tags.size());
		return tags.get(i);
	}

	public Type getRandomType() {
		int i = new Random().nextInt(types.size());
		return types.get(i);
	}

	public User getLastUser() {
		if (users.size() > 0)
			return users.get(users.size() - 1);
		return generateNewUser();
	}

	public Picture getRandomPicture() {
		int i = new Random().nextInt(pictures.size());
		return pictures.get(i);
	}

	public User getRandomUser() {
		int i = new Random().nextInt(users.size());
		return users.get(i);
	}
}
