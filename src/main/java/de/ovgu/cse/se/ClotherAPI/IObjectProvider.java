package de.ovgu.cse.se.ClotherAPI;

import de.ovgu.cse.se.ClotherAPI.exceptions.*;
import de.ovgu.cse.se.ClotherAPI.models.*;

import java.util.List;

public interface IObjectProvider {

	void addOccasion(Occasion occasion) throws OccasionNotAddedException,
			UserNotAuthenticatedException;

	/**
	 * speichert ein Bild in der Datenbank ab.
	 * 
	 * @param picture
	 *            Bild das hochgeladen werden soll
	 * @throws PictureNotAddedException
	 * @throws UserNotAuthenticatedException
	 */
	void addPicture(Picture picture) throws PictureNotAddedException,
			UserNotAuthenticatedException;

	void addTag(Tag tag) throws TagNotAddedException,
			UserNotAuthenticatedException;

	void addType(Type type) throws TypeNotAddedException,
			UserNotAuthenticatedException;

	/**
	 * legt einen neuen User in der Datenbank an
	 * 
	 * @param user
	 *            Userobjekt, welches in der Datenbank gespeichert werden soll
	 * @throws UserNotAddedException
	 */
	void addUser(User user) throws UserNotAddedException;

	/**
	 * Adds a vote to the database.
	 * 
	 * @param vote
	 *            the vote that should be added to the picture
	 * @param picture
	 *            the picture, the vote should be added to
	 * @throws VoteNotAddedException
	 * @throws UserNotAuthenticatedException
	 */
	void addVote(Vote vote) throws VoteNotAddedException,
			UserNotAuthenticatedException;

	/**
	 * überprüft, ob die angegebenen Benutzerdaten korrekt sind. Wenn ja, wir
	 * der Benutzer zurückgegeben. Ansonsten eine Exception
	 * 
	 * @param email
	 *            email address of the user
	 * @param password
	 *            user password which will be encrypted and checked against the
	 *            encrypted version in the database
	 * @return the user object of the authenticated user
	 * @throws UserdataNotCorrectException
	 */
	User authenticate(String email, String password)
			throws UserdataNotCorrectException;

	void closeConnection();

	/**
	 * löscht das Entsprechende Bild aus der Datenbank
	 * 
	 * @param ID
	 *            des Bildes
	 * @throws PictureNotDeletedException
	 * @throws UserNotAuthenticatedException
	 */
	void deletePicture(Picture picture) throws PictureNotDeletedException,
			UserNotAuthenticatedException;

	/**
	 * löscht den authentifizierten User aus der Datenbank
	 * 
	 * @return true, wenn Account erfolgreich gelöscht wurde
	 * @throws UserNotAuthenticatedException
	 */
	void deleteUser() throws UserNotDeletedException,
			UserNotAuthenticatedException;

	void deleteVote(Vote vote) throws VoteNotDeletedException,
			UserNotAuthenticatedException;

	Occasion getOccasion(String occasionname) throws OccasionNotFoundException,
			UserNotAuthenticatedException;

	List<Occasion> getOccasions() throws OccasionNotFoundException,
			UserNotAuthenticatedException;

	/**
	 * Deprecated, as own votes can be directly accessed using the user object
	 * and the associated votes list.
	 * 
	 * @return
	 * @throws VoteNotFoundException
	 * @throws UserNotAuthenticatedException
	 */
	@Deprecated
	List<Vote> getOwnVotes() throws VoteNotFoundException,
			UserNotAuthenticatedException;

	Picture getPicture(Long pictureid) throws PictureNotFoundException,
			UserNotAuthenticatedException;

	public Picture getPicture(String picturename)
			throws PictureNotFoundException, UserNotAuthenticatedException;

	List<Picture> getPictures() throws PictureNotFoundException,
			UserNotAuthenticatedException;

	Tag getTag(String tagname) throws TagNotFoundException,
			UserNotAuthenticatedException;

	List<Tag> getTags() throws TagNotFoundException,
			UserNotAuthenticatedException;

	Type getType(String typename) throws TypeNotFoundException,
			UserNotAuthenticatedException;

	List<Type> getTypes() throws TypeNotFoundException,
			UserNotAuthenticatedException;

	User getUser();

	Vote getVote(Long voteid) throws VoteNotFoundException,
			UserNotAuthenticatedException;

	void updateOccasion(Occasion occasion) throws OccasionNotUpdatedException,
			UserNotAuthenticatedException;

	void updatePicture(Picture picture) throws PictureNotUpdatedException,
			UserNotAuthenticatedException;

	void updateTag(Tag tag) throws TagNotUpdatedException,
			UserNotAuthenticatedException;

	void updateType(Type type) throws TypeNotUpdatedException,
			UserNotAuthenticatedException;

	/**
	 * speichert Änderungen am authentifizierten User in der Datenbank
	 * 
	 * @param Objekt
	 *            User, ID des Nutzers
	 * @return true, wenn Account erfolgreich geändert wurde
	 * @throws UserNotAuthenticatedException
	 */
	void updateUser() throws UserNotUpdatedException,
			UserNotAuthenticatedException;

	void updateVote(Vote vote) throws VoteNotUpdatedException,
			UserNotAuthenticatedException;

}